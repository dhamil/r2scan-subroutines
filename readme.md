# r<sup>2</sup>SCAN subroutines and patch files

## Maintainers: James W. Furness, Aaron D. Kaplan
***

This repo contains files needed to implement the novel and efficient r2 SCAN meta-GGA,

J. W. Furness, A. D. Kaplan, J. Ning, J. P. Perdew, and J. Sun,
"Accurate and Numerically Efficient r<sup>2</sup>SCAN Meta-Generalized Gradient Approximation",
J. Phys. Chem. Lett. **11**, 8208-8215 (2020). [DOI: 10.1021/acs.jpclett.0c02405]
https://doi.org/10.1021/acs.jpclett.0c02405

in popular electronic structure codes (VASP, Turbomole) as well as general purpose routines.

***
The directory "vasp_patch_files" contains patches for
  VASP 5.4.4 : metagga544.diff
  VASP 6.1 : metagga6.diff

To patch a version of VASP, simply run\
*patch < metagga[ver].diff* \
in the src directory of your VASP install, where [ver] = 544 or 6.
***

The files `xscan.f` and `cscan.f` implement the r<sup>2</sup>SCAN functional as generic Fortran routines returning energy densities and first derivatives as example implementations.

The r<sup>2</sup>SCAN functional is also available in the popular [LibXC](https://gitlab.com/libxc/libxc) and [XCFun](https://github.com/dftlibs/xcfun) libraries.

For the dispersion corrected r<sup>2</sup>SCAN-D4 subroutines, please see<br/>
https://github.com/awvwgk/r2scan-d4-paper
<br/>and the original reference

S. Ehlert, U. Huniar, J. Ning, J.W. Furness, J. Sun, A.D. Kaplan, J.P. Perdew, and J.G. Brandenburg,
"r<sup>2</sup>SCAN-D4: Dispersion corrected meta-generalized gradient approximation for general chemical applications",
J. Chem. Phys. **154**, 061101 (20201) [DOI: 10.1063/5.0041008].<br/>
https://doi.org/10.1063/5.0041008
***

## Note on r<sup>2</sup>SCAN derivation, development of r++SCAN and r<sup>4</sup>SCAN

For the derivation of the r<sup>2</sup>SCAN meta-GGA, as well as two new meta-GGAs, see the preprint on arXiv:<br/>
https://arxiv.org/abs/2110.00647
<br/>The two new functionals, called r++SCAN (a step between rSCAN and r<sup>2</sup>SCAN) and r<sup>4</sup>SCAN (adds the fourth-order gradient expansion constraint to r<sup>2</sup>SCAN), are not recommended for general-purpose calculations.
**We recommend using r<sup>2</sup>SCAN for general-purpose calculations.**

The subroutines maintained here, and in LibXC, contain up-to-date parameters for these two new functionals.

***

## Reference atomic energies

If you're interested in writing your own r<sup>2</sup>SCAN, r++SCAN, or r<sup>4</sup>SCAN subroutines, the following tables present reference self-consistent atomic exchange-only and exchange-correlation energies using these functionals.
These can be used to validate independent implementations of the meta-GGAs.

All calculations performed here used the cc-pVTZ basis set [a,b] and the "reference" level Turbomole grid.

All values are given in Hartree atomic energy units, 1 E<sub>h</sub> ~= 27.211386 eV ([NIST value](https://physics.nist.gov/cgi-bin/cuu/Value?eqhrev))

[a] T.H. Dunning, J. Chem. Phys. **90**, 1007 (1989). [DOI:10.1063/1.456153](https://doi.org/10.1063/1.456153)</br>
[b] D.E. Woon and T.H. Dunning, J. Chem. Phys. **103**, 4572 (1995). [DOI:10.1063/1.470645](https://doi.org/10.1063/1.470645)

### Exchange-only reference-energies

**NB**, here, total energies E<sub>tot</sub> are calculated **only** with the exchange energy, i.e. E<sub>c</sub>=0 and the corresponding spin-potentials v<sub>c</sub>=0.

Use these two tables to validate an exchange functional/potential.

**Nitrogen atom**

|  | SCAN | rSCAN | r<sup>2</sup>SCAN |
|:---:|:---:|:---:|:---:|
| E<sub>tot</sub> | -54.40541174994 | -54.41398626268 | -54.39774875432 |
| E<sub>x</sub> | -6.597139583375 | -6.604261530513 | -6.586631390511 |

**Neon atom**

|  | SCAN | rSCAN | r++SCAN | r<sup>2</sup>SCAN | r<sup>4</sup>SCAN |
|:---:|:---:|:---:|:---:|:---:|:---:|
| E<sub>tot</sub> | -128.5891914977 | -128.6082003154 | -128.6015518 | -128.5698768302 | -128.5718443 |
| E<sub>x</sub> | -12.143536468556 | -12.160698876055 | -12.15421314 | -12.11878636737 | -12.12041514 |


### Exchange-correlation reference-energies

**NB**, here, total energies E<sub>tot</sub> are calculated using the exchange-correlation energy.

After evaluating an exchange functional/potential, use these two tables to validate the correlation functional/potential.

**Nitrogen atom**

|  | SCAN | rSCAN | r<sup>2</sup>SCAN |
|:---:|:---:|:---:|:---:|
| E<sub>tot</sub> | -54.58565736367 | -54.59430112747 | -54.57900797069 |
| E<sub>xc</sub> | -6.782282570589 | -6.790099568534 | -6.773066377305 |

**Neon atom**

|  | SCAN | rSCAN | r++SCAN | r<sup>2</sup>SCAN | r<sup>4</sup>SCAN |
|:---:|:---:|:---:|:---:|:---:|:---:|
| E<sub>tot</sub> | -128.9340794821 | -128.9541235246 | -128.9480969 | -128.9168416529 | -128.9188216 |
| E<sub>xc</sub> | -12.49701065638 | -12.515767373154 | -12.50979948 | -12.474955269761 | -12.4769353 |

## License

<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/p/zero/1.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 1.0 Universal (CC0 1.0) Public Domain Dedication</a>.
